var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

/*
 create or replace view account_view as
 select s.*, a.street, a.zip_code from account s
 join address a on a.address_id = s.address_id;

 */

exports.getAll = function(callback) {
    var query = 'SELECT a.first_name, a.last_name, r.resume_id, r.resume_name FROM resume r ' +
    'inner JOIN account a on a.account_id = r.account_id;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.add = function(account_id, callback) {
    var query = 'SELECT * from account where account_id = ?';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.schoolByUser = function(account_id, callback) {
    var query = 'SELECT c.school_id, c.school_name FROM school c ' +
        'join account_school ac on ac.school_id=c.school_id ' +
        'where ac.account_id= ?';

    var queryData = [account_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    // FIRST INSERT THE COMPANY
    var query = 'INSERT INTO resume (resume_name, account_id) VALUES (?, ?)';

    var queryData = [params.resume_name, params.account_id];

    connection.query(query, queryData, function(err, result) {

        // THEN USE THE COMPANY_ID RETURNED AS insertId AND THE SELECTED ADDRESS_IDs INTO COMPANY_ADDRESS
        var resume_id = result.insertId;

        // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
        var query = 'INSERT INTO resume_company (resume_id, company_id) VALUES ?';

        // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
        var resumeCompanyData = [];
        if (params.company_id.constructor === Array) {
            for (var i = 0; i < params.company_id.length; i++) {
                resumeCompanyData.push([resume_id, params.company_id[i]]);
            }
        }
        else {
            resumeCompanyData.push([resume_id, params.company_id]);
        }

        // NOTE THE EXTRA [] AROUND companyAddressData
        connection.query(query, [resumeCompanyData], function(err, result){
            
            var resume_id = resumeCompanyData[0][0]
            var query = 'INSERT INTO resume_school (resume_id, school_id) VALUES ?';

            var resumeSchoolData = [];
            if (params.school_id.constructor === Array) {
                for (var i = 0; i < params.school_id.length; i++) {
                    resumeSchoolData.push([resume_id, params.school_id[i]]);
                }
            }
            else {
                resumeSchoolData.push([resume_id, params.school_id]);
            }
            
            connection.query(query,[resumeSchoolData], function (err, result) {
                var resume_id = resumeSchoolData[0][0]
                var query = 'INSERT INTO resume_skill (resume_id, skill_id) VALUES ?';

                var resumeskillData = [];
                if (params.skill_id.constructor === Array) {
                    for (var i = 0; i < params.skill_id.length; i++) {
                        resumeskillData.push([resume_id, params.skill_id[i]]);
                    }
                }
                else {
                    resumeskillData.push([resume_id, params.skill_id]);
                }

                connection.query(query,[resumeskillData], function (err, result) {
                    callback (err, resume_id);
                });
            });
        });
    });
};

exports.edit = function(resume_id, callback) {
    var query = 'CALL resume_getinfo(?)';
    var queryData = [resume_id];

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

var resumeCompanyInsert = function(resume_id, companyIdArray, callback){
    // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
    var query = 'INSERT INTO resume_company (resume_id, company_id) VALUES ?';

    // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
    var resumeCompanyData = [];
    if (companyIdArray.constructor === Array) {
        for (var i = 0; i < companyIdArray.length; i++) {
            resumeCompanyData.push([resume_id, companyIdArray[i]]);
        }
    }
    else {
        resumeCompanyData.push([resume_id, companyIdArray]);
    }
    connection.query(query, [resumeCompanyData], function(err, result){
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.resumeCompanyInsert = resumeCompanyInsert;

var resumeSkillInsert = function(resume_id, skillIdArray, callback){
    // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
    var query = 'INSERT INTO resume_skill (resume_id, skill_id) VALUES ?';

    // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
    var resumeSkillData = [];
    if (skillIdArray.constructor === Array) {
        for (var i = 0; i < skillIdArray.length; i++) {
            resumeSkillData.push([resume_id, skillIdArray[i]]);
        }
    }
    else {
        resumeSkillData.push([resume_id, skillIdArray]);
    }
    connection.query(query, [resumeSkillData], function(err, result){
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.resumeSkillInsert = resumeSkillInsert;

var resumeSchoolInsert = function(resume_id, schoolIdArray, callback){
    // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
    var query = 'INSERT INTO resume_school (resume_id, school_id) VALUES ?';

    // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
    var resumeSchoolData = [];
    if (schoolIdArray.constructor === Array) {
        for (var i = 0; i < schoolIdArray.length; i++) {
            resumeSchoolData.push([resume_id, schoolIdArray[i]]);
        }
    }
    else {
        resumeSchoolData.push([resume_id, schoolIdArray]);
    }
    connection.query(query, [resumeSchoolData], function(err, result){
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.resumeSchoolInsert = resumeSchoolInsert;


var resumeCompanyDeleteAll = function(resume_id, callback){
    var query = 'DELETE FROM resume_company WHERE resume_id = ?';
    var queryData = [resume_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.resumeCompanyDeleteAll = resumeCompanyDeleteAll;

var resumeSkillDeleteAll = function(resume_id, callback){
    var query = 'DELETE FROM resume_skill WHERE resume_id = ?';
    var queryData = [resume_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.resumeSkillDeleteAll = resumeSkillDeleteAll;

var resumeSchoolDeleteAll = function(resume_id, callback){
    var query = 'DELETE FROM resume_school WHERE resume_id = ?';
    var queryData = [resume_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.resumeSchoolDeleteAll = resumeSchoolDeleteAll;




exports.update = function(params, callback) {
    var query = 'UPDATE resume SET resume_name = ? WHERE resume_id = ?';
    var queryData = [params.resume_name, params.resume_id];

    connection.query(query, queryData, function(err, result) {
        //delete company_address entries for this company
        resumeCompanyDeleteAll(params.resume_id, function(err, result){

            if(params.company_id != null) {
                //insert resume_company ids
                resumeCompanyInsert(params.resume_id, params.company_id, function(err, result){
                    resumeSkillDeleteAll(params.resume_id, function(err, result){

                        if(params.skill_id != null) {
                            //insert resume_skill ids
                            resumeSkillInsert(params.resume_id, params.skill_id, function(err, result){
                                resumeSchoolDeleteAll(params.resume_id, function(err, result){

                                    if(params.company_id != null) {
                                        //insert resume_company ids
                                        resumeSchoolInsert(params.resume_id, params.school_id, function(err, result){
                                            callback(err, result);
                                        });}
                                    else {
                                        callback(err, result);
                                    }
                                });
                            });}
                        else {
                            callback(err, result);
                        }
                    });
                });}
            else {
                callback(err, result);
            }
        });

    });
};