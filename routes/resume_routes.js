var express = require('express');
var router = express.Router();
var resume_dal = require('../model/resume_dal');
var account_dal = require('../model/account_dal');
var company_dal = require('../model/company_dal');
var skill_dal = require('../model/skill_dal');



// View All resumes
router.get('/all', function(req, res) {
    resume_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('resume/resumeViewAll', { 'result':result });
        }
    })
});

router.get('/add', function(req, res) {
    if (req.query.account_id == null) {
        res.send('An account id is required');
    }
    else {
        resume_dal.add(req.query.account_id, function (err, account) {
            skill_dal.getByUser(req.query.account_id, function (err, skill) {
                company_dal.getByUser(req.query.account_id, function (err, company) {
                    resume_dal.schoolByUser(req.query.account_id, function (err, school) {
                        res.render('resume/add/addResume', {account: account[0], company: company, skill: skill, school: school});
                    });
                });
            });
        });
    }
});

// Select user
router.get('/add/selectuser', function(req, res) {
    account_dal.getAll(function(err, result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('resume/add/selectuser', { 'account':result });
        }
    })
});

router.post('/add/submit', function(req, res){
    // simple validation
    if(req.body.resume_name == null) {
        res.send('a resume Name must be provided.');
    }
    if(req.body.company_id == null) {
        res.send('A company must be provided.');
    }
    if(req.body.school_id == null) {
        res.send('A school must be provided.');
    }
    else if(req.body.skill_id == null) {
        res.send('At least one skill must be selected');
    }
    else {
        // passing all the query parameters (req.body) to the insert function instead of each individually
        resume_dal.insert(req.body, function(err,resume_id) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                resume_dal.edit(resume_id, function(err, result) {
                    res.render('resume/resumeUpdate', {resume: result[0][0], company: result[1], skill: result[2], school: result[3], was_successful: true});
                });
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.resume_id == null) {
        res.send('A resume id is required');
    }
    else {
        resume_dal.edit(req.query.resume_id, function(err, result){
            res.render('resume/resumeUpdate', {resume: result[0][0], company: result[1], skill: result[2], school: result[3]});
        });
    }

});

router.get('/update', function(req, res) {
    resume_dal.update(req.query, function(err, result){
        res.redirect(302, '/resume/all');
    });
});

module.exports = router;